package org.atp.annotation.annotation_processing;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ExecutableType;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@SupportedAnnotationTypes("org.atp.annotation.annotation_processing.BuilderProperty")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class BuilderPropertyProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
        for (TypeElement annotation: annotations){
            List<Element> annotatedElements = retrieveSetters(annotation, roundEnvironment);
            String className = ((TypeElement) annotatedElements.get(0).getEnclosingElement()).getQualifiedName().toString();
            Map<String, String> settersParameters = retrieveParameterTypes(annotatedElements);
            try {
                writeBuilderFile(className, settersParameters);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private List<Element> retrieveSetters(TypeElement annotation, RoundEnvironment roundEnvironment){
        Set<? extends Element> annotatedElements = roundEnvironment.getElementsAnnotatedWith(annotation);
        Map<Boolean, List<Element>> methods = annotatedElements.stream().collect(
                Collectors.partitioningBy(
                        element ->
                                ((ExecutableType) element.asType()).getParameterTypes().size() == 1 &&
                                element.getSimpleName().toString().startsWith("set")
                )
        );
        messageForInvalid(methods.get(false));
        return methods.get(true);
    }

    private Map<String, String> retrieveParameterTypes(List<Element> setters){
        Map<String, String> settersParameters = new HashMap<>();
        for(Element setter: setters){
            settersParameters.put(
                    setter.getSimpleName().toString(),
                    ((ExecutableType) setter.asType()).getParameterTypes().get(0).toString()
                    );
        }
        return settersParameters;
    }

    private void messageForInvalid(List<Element> invalids){
        for(Element el: invalids){
            processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "Error applying element", el);
        }
    }

    private void writeBuilderFile(String className, Map<String, String> setters) throws IOException {
        String packageName = null;
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.substring(0, lastDot);
        }

        String simpleClassName = className.substring(lastDot + 1);
        String builderClassName = className + "AutomatedBuilder";
        String builderSimpleClassName = builderClassName.substring(lastDot + 1);

        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(builderClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {

            if (packageName != null) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();
            }

            out.print("public class ");
            out.print(builderSimpleClassName);
            out.println(" {");
            out.println();

            out.print("    private ");
            out.print(simpleClassName);
            out.print(" object = new ");
            out.print(simpleClassName);
            out.println("();");
            out.println();

            out.print("    public ");
            out.print(simpleClassName);
            out.println(" build() {");
            out.println("        return object;");
            out.println("    }");
            out.println();

            setters.entrySet().forEach(setter -> {
                String methodName = setter.getKey();
                String argumentType = setter.getValue();

                out.print("    public ");
                out.print(builderSimpleClassName);
                out.print(" ");
                out.print(methodName);

                out.print("(");

                out.print(argumentType);
                out.println(" value) {");
                out.print("        object.");
                out.print(methodName);
                out.println("(value);");
                out.println("        return this;");
                out.println("    }");
                out.println();
            });

            out.println("}");

        }
    }

}
