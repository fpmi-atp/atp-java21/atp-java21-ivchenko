package org.atp.annotation;

import org.atp.annotation.annotation_processing.BuilderProperty;

public class Person {
    private int age;
    private String name;
    private String surname;

    public String getSurname() {
        return surname;
    }

    @BuilderProperty
    public void setSurname(String surname) {
        this.surname = surname;
    }
    @BuilderProperty
    public int getAge() {
        return age;
    }

    @BuilderProperty
    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    @BuilderProperty
    public void setName(String name) {
        this.name = name;
    }
}
