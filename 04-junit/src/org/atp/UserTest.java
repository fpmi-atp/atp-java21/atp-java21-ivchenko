package org.atp;

import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserTest {
    private ArrayList<User> db = new ArrayList<>();

    @BeforeClass
    public static void hello(){
        System.out.println("Testing started");
    }

    @AfterClass
    public static void bye(){
        System.out.println("Testing finished");
    }

    @Before
    public void fillDB(){
        System.out.println("Test started");
        db.add(new User("Евгений", 35, Sex.MALE));
        db.add(new User("Марина", 34, Sex.FEMALE));
        db.add(new User("Алина", 7, Sex.FEMALE));
    }

    @After
    public void clearDB(){
        System.out.println("Test finished. Cleaning up...");
        db.clear();
        User.resetDb();
    }

    @Test
    public void getAllUsers() {
        ArrayList<User> actualUsers = (ArrayList<User>) db.clone();
        actualUsers.add(new User("Евгений1", 35, Sex.MALE));
        actualUsers.add(new User("Марина1", 34, Sex.FEMALE));
        actualUsers.add(new User("Алина1", 7, Sex.FEMALE));

        List<User> expectedUsers = User.getAllUsers();
//        actualUsers.remove(0);
        Assert.assertEquals(expectedUsers, actualUsers);
    }

    @Ignore
    public void testGetAllUsers() {
        List<User> expectedUsers = User.getAllUsers();
        Assert.assertNotNull(expectedUsers);
    }

    @Test(expected = NullPointerException.class)
    public void testNull(){
        List<User> expectedNull = null;
        expectedNull.add(new User("Алина", 7, Sex.FEMALE));
        Assert.assertNull(expectedNull);
    }

}
