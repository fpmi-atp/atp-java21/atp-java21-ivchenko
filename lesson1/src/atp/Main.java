package atp;

import java.lang.Math;
import java.util.Arrays;

public class Main {
    public static void test2(){
//        int i = 123456789;
//        float f = i;
//        double d = i;
//        short s = (short) i;
//        System.out.println("int: " + i);
//        System.out.println("float: " + f);
//        System.out.println("double: " + d);
//        System.out.println("short: " + s);

//        int a=1;
//        int b=0;
//        int c=a/b;
//        System.out.println(c);

//        double a=1;
//        double b=0;
//        double c=a/b;
//
//        System.out.println(c);
//        System.out.println("c+1 =" + (c + 1));
//
//        System.out.println("+0.0 == -0.0 : " + (0.0 == -0.0));
//        System.out.println("a/(+0.0) = " + (a/(+0.0)));
//        System.out.println("a/(-0.0) = " + (a/(-0.0)));

        double a=0;
        double b=0;
        double c=a/b;
        System.out.println("c+0 =" + (c + 0));
        System.out.println("c<0 =" + (c < 0));
        System.out.println("c<=0 =" + (c <= 0));
        System.out.println("c>0 =" + (c > 0));
        System.out.println("c>=0 =" + (c >= 0));
        System.out.println("c==0 =" + (c == 0));
        System.out.println("c!=0 =" + (c != 0));
        System.out.println("c==c =" + (c == c)); // :)
        System.out.println("c!=c =" + (c != c)); // :)

        Double d = 2.0;
        Double d2 = Double.valueOf(2.0);
        double d3 = d2.doubleValue();
    }

    public static void main(String[] args) {
        test2();

        Integer i = 2;
        Integer i2 = new Integer(3);
        Integer i3 = Integer.valueOf(3);

        int[] a = {1,2,3,4,5};
        System.out.println(a.toString());
        System.out.println(Arrays.toString(a));
//        System.out.println("Hello world!");
//
//        StringBuilder sb = new StringBuilder("My string");
//        sb.append(" ").append("hello");
//        System.out.println(sb.toString());
//
//        String a = "sfsdf";
//        String b = a;
//        System.out.println(a == b);
//        a += "";
//        System.out.println(a == b);
//        System.out.println(b);
//        System.out.println(a);
//
//        System.gc();
//
//        long bigValue = 99L; // в переменную long пишем long : Ok!
//        int squashed = (int)bigValue; // long преобразуем в int : Ok!
//        long bigval = 6; // в переменную long пишем int: Ok!
////        int smallval = 99L; // в переменную int пишем long : Ошибка!
//
//        float x = 0.05f;
//        System.out.println((int)x);
    }
}
