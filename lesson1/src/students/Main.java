package students;

public class Main {
    public static void main(String[] args) {
        Student st = new Student("Oleg", "Ivchenko", "o@velkerr.ru");
        Student st2 = new Student();
//        st.name = "Victor";
//        System.out.println(st.name);

        Group gr = new Group();
        System.out.println(gr);
        gr.addStudent(new Student("Oleg", "Ivchenko", "o@velkerr.ru"));
        System.out.println(gr);
        gr.deleteStudent(st);
        System.out.println(gr);
    }
}
