class A {
    static int a1=printStringReturnInt("a1");

    static {
        printStringReturnInt("static block");
    }

    A() {
        printStringReturnInt("Constructor");
    }

    static int a2 = printStringReturnInt("a2");

    public static int printStringReturnInt(String s) {
        System.out.println(s);
        return 0;
    }
}

public class Main {
    static {
        System.out.println("Before main");
    }

    public static void main(String[] args) {
        System.out.println("Begin of main");
        A a = new A();
        System.out.println("End of main");
    }

    static {
        System.out.println("After main");
    }
}